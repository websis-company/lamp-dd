<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Docker + PHP</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<h1>Conenedor PHP 5.6-apache</h1>
	<p>
		Ver imagenes<br>
		<code>
			docker images
		</code>
	</p>
	<p>	
		Ver Contenedores <br>
		<code>
			docker ps //Contenedores corriendo
			<br>
			docker ps -a //Contenedores detenidos
		</code>
	</p>
	<p>
		Eliminar contenedores<br>
		<code>
			docker rm $(docker ps -aq)
		</code>
	</p>

	<hr>
	<p>
		Docker-Compose [INICIALIZAR]<br>
		<code>
			docker-compose up -d //Inicia docker compose 
		</code>
		<br>
		<br>
		Docker-Compose [CONSTRUIR]
		<code>
			docker-compose up -d --build //Construye docker-compose.yaml
		</code>
	</p>
	<p>
		Docker-Compose [DETENER]
		<code>
			docker-compose down
		</code>
	</p>
</body>
</html>