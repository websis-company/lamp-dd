<?php 
die();

include_once 'bo/common.php';
$id_product  = $_REQUEST['id_product'];
$receta      = eCommerce_SDO_recetario::Loadrecetario($id_product);

$receta_name        = $receta->getNombre();
$receta_img         = $receta->getUrlArrayImages('large',0);
$receta_des         = nl2br($receta->getDescripcion());
$receta_video       = $receta->getVideo();
$receta_pdf         = $receta->getPdf();
$receta_preparacion = $receta->getTiempoPreparacion();
?>

<div class="modal-header">
	<h2 class="modal-title"><?php echo $receta_name; ?></h2>
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
</div>

<div class="modal-body">
	<div class="product-modal-image">
		<img src="<?php echo $receta_img; ?>" alt="<?php echo $receta_name;?>" class="img-responsive">
	</div>
	<br>
	<div class="product-modal-info">
		<div class="recipe-item-text"><?php echo $receta_des; ?></div>
		<div class="recipe-item-features">
			<?php 
			if(!empty($receta_video)){
			?>
			<a href="http://<?php echo $receta_video ?>" target="_blank">
                <img src="images/video.png" alt="PDF" class="img-centrada">
            </a>
			<?php	
			}else{
				if(!empty($receta_pdf[0])){
					$file_url = ABS_HTTP_URL.'bo/file.php?id='.$receta_pdf[0];
				?>
		            <a href="<?php echo $file_url ?>" target="_blank">
		                <img src="images/pdf.png" alt="PDF" class="img-centrada">
		            </a>
				<?php 
				}//end if
			}//end if
			?>

			
			<span class="recipe-item-time">
                <img src="images/reloj.png" alt="tiempo" class="img-centrada"><span><?php echo $receta_preparacion; ?></span>
            </span>
            <!--<div class="col-xs-4">
                <a href="" class="heart">
                	<i class="fa fa-heart" aria-hidden="true"></i>
                	0
                </a>
            </div>-->
		</div>
	</div>
</div>
 