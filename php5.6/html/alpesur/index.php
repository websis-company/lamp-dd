
<?php
	$header_title = 'Alimento balanceado para animales | Alpesur';
	$header_description = 'Alimento balanceado para animales, Cerdos, Pollo de engorda, Bovinos, Pavos, Ovinos, Caballos y más. El Alimento ideal para cada etapa, nutrición animal que marca la diferencia - Alpesur.';
?>
<?php include('includes/head.php'); ?>
	<!-- Revolution Slider -->
	<link rel="stylesheet" href="plugins/revolution-slider/settings.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="plugins/owl-carousel/owl.transitions.css">
	<link rel="stylesheet" href="plugins/owl-carousel/owl-single.css">
	<link rel="stylesheet" href="plugins/owl-carousel/owl-multi.css">
<?php include('includes/header.php'); ?>
<?php include("includes/nav.php"); ?>

<!--Microdatos-->
<div itemscope itemtype="http://schema.org/WebPage" style="display:none">
	<span itemprop="description">Alimento balanceado para animales | Alpesur</span>
	<span itemprop="image"><?=ABS_HTTP_URL?>images/logo_seo.jpg</span>
	<span itemprop="name">Alpesur, S.A. de C.V.</span>
	<span itemprop="url"><?=ABS_HTTP_URL?></span>
	<div itemprop="author" itemscope itemtype="http://schema.org/Corporation" >
		<span itemprop="description">Alimento balanceado para animales | Alpesur</span>
		<span itemprop="image"><?=ABS_HTTP_URL?>images/logo_seo.jpg</span>
		<span itemprop="name">Alpesur, S.A. de C.V.</span>
		<span itemprop="url"><?=ABS_HTTP_URL?></span>
		<div itemprop="contactPoint" itemscope itemtype="http://schema.org/ContactPoint">
			<span itemprop="telephone">+52 01 271 717 0300</span>
			<span itemprop="contactType">sales</span>
		</div>
		<div itemprop="Brand" itemscope itemtype="http://www.schema.org/Brand">
			<span itemprop="name">Alpesur, S.A. de C.V.</span>
			<span itemprop="url"><?=ABS_HTTP_URL?></span>
			<span itemprop="logo"><?=ABS_HTTP_URL?>images/logo_seo.jpg</span>
		</div>
		<div itemprop="location" itemscope itemtype="http://schema.org/Place" >
			<span itemprop="description">Alimento balanceado para animales | Alpesur</span>
			<span itemprop="image"><?=ABS_HTTP_URL?>images/logo_seo.jpg</span>
			<span itemprop="name">Alpesur, S.A. de C.V.</span>
			<span itemprop="url"><?=ABS_HTTP_URL?></span>
			<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
				<span itemprop="streetAddress">Km. 335 Carretera Federal Fortín – Córdoba s/n C.P. 94540, Colonia San Nicolás, Córdoba</span>
				<span itemprop="addressLocality">Veracruz</span>
				<span itemprop="addressRegion">México</span>
				<span itemprop="postalCode">94540</span>
			</div>
			<div itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
				<meta itemprop="latitude" content="18.901355" />
				<meta itemprop="longitude" content="-96.976051" />
			</div>
		</div> <!--Place-->
	</div><!--Corporation-->
</div><!--Web Page-->
<!--Microdatos-->


	<div id="is-home"></div>
	<main role="main">
		<!-- Main slider. Begin -->
		<section id="section-home" class="main-slider-fullscreen">
			<?php 
			include_once('banner_home.php');
			?>
		</section>
		<!-- Main slider. End -->
		<!-- About us short. Begin -->
		<section class="food-home bg-cover-center" style="background-image: url(images/back-about.jpg);">
			<div class="re-cien">
				<div class="row">
					<div class="col-lg-6">
						<!-- <img src="images/food-service_home.jpg" class="img-cien"> -->
					</div>
					<div class="col-lg-6 item-food">
						<div class="title-icon">
							<h3 class="">Nosotros</h3>
						</div>
						<p class="font-white">
							Somos una empresa en nutrición animal 100% mexicana, comprometida por más de 4 décadas en fortalecer buenas relaciones.
						</p>
						<a href="nosotros.php" class="button-flat" title="Nosotros">ver más</a>
					</div>
				</div>
			</div>
		</section>
		<section class="re-cien">
			<div class="about-us-home">
				<div class="about-us-home-summary">
					<div>
						<h2 class="about-us-home-title title-icon">Nuestros Productos</h2>
						<p class="font-tertiary">Proporcionamos a nuestros clientes los mejores productos en calidad y servicio.</p>
						<a href="1/producto/cerdos/1/" class="button-flat" title="Nuestros Productos">Leer más</a>
					</div>
				</div>
				<div class="about-us-home-years">
					<img src="images/about-us-home-years.png" alt="Tradición desde 1920" class="about-us-home-years-image">
				</div>
			</div>
		</section>
		<section class="food-cercania bg-cover-center" style="background: url(images/back-cercanias.jpg) bottom center no-repeat; background-size:cover;">
			<div class="re-cien">
				<div class="row">
					
					<div class="col-lg-6 item-food" style=" background-color:rgba(253,238,138,0.3);">
						<div class="title-icon">
							<h3 class="font-tertiary">Cercanía que nutre</h3>
						</div>
						<p class="font-tertiary">
							Más que una frase, una forma de vivir todos los días.
						</p>
						<a href="cercania.php" class="button-flat" title="Cercanía que nutre">ver más</a>
					</div>
                    
                    <div class="col-lg-6">
						<img src="images/cercania.png" alt="Cercanía que nutre"> 
					</div>
				</div>
			</div>
		</section>	
		<!-- Social. End -->
	</main>
<?php 
$ancla = !isset($_REQUEST['anchor']) ? "" : $_REQUEST['anchor'] ;

$GLOBALS['scripts'] .= '
	<!-- RevoSlider -->
	<script src="plugins/revolution-slider/jquery.themepunch.tools.min.js"></script>
	<script src="plugins/revolution-slider/jquery.themepunch.revolution.min.js"></script>
	<!-- OWL Carousel -->
	<script src="plugins/owl-carousel/owl.carousel.js"></script>
	<script src="plugins/owl-carousel/owl.carousel.vertical.js"></script>
	<script src="css-js/home.js"></script>
	<script>
		$(function(){ 
			setTimeout(function(){
				var strAncla = "#'.$ancla.'";
				$("body,html").stop(true,true).animate({
					scrollTop: $(strAncla).offset().top;
				},1000);
			},0)
		})
	</script>
'; ?>
<?php include('includes/footer.php'); ?>