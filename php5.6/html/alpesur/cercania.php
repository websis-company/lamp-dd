<?php
	$header_title = 'Cercanía que nutre | Alpesur';
	$header_description = 'Asesorar con calidad, servir con valores, Generar confianza - Alpesur';
?>
<?php include('includes/head.php'); ?>
<?php include('includes/header.php'); ?>
<?php include("includes/nav.php"); ?>
<link rel="stylesheet" href="css-js/estilos-nvo.css">

	<main role="main">
		<!--div class="container sinPadLat"  style="background-image:url(images/back-nosotros.jpg); width:100%" -->
        <h1 style="display: none;">Cercanía que nutre - Alpesur</h1>
      <div class="re-cien int-top" style="background:url(images/bg-cercania.jpg) top center no-repeat; background-size: cover;">
		
        	<div class="re-central">
            	
                <div class="col-lg-12 padding-b-40" align="center">
                	<img src="images/cercania.png" alt="Cercanía que nutre" >                	
                </div>
                <div class="col-lg-12 padding-b-40" align="center">
                	<img src="images/cercania-que-nutre.png" alt="Cercanía que nutre" class="img-cercania">                	
                </div>
                
                <div class="col-lg-12">
                	<p class="cercania">Cercanía que nutre es más que una frase, es como deseamos ser percibidos por nuestros diferentes públicos, es una forma de vivir todos los días, de relacionarnos con nuestros clientes y de mantener cercanía y compromiso para...</p>
                </div>
                 <div class="col-lg-12" align="center">
                	<img src="images/cara.png" alt="Cercanía que nutre" >                	
                </div>
                <div class="col-lg-12" align="center">
                	<img src="images/fortalecer.png" alt="Cercanía que nutre" >                	
                    <p class="cercania padding-30">Esto lo conseguimos al:</p>
                </div>
                
                <div class="col-lg-12 padding-25" align="center">
                	<div class="col-lg-4" align="center">
	                	<img src="images/asesorar.png" alt="Cercanía que nutre" >
                        <p class="cercania-1">Los mejores resultados no se logran solo con productos de la mejor calidad, deben venir acompañados del mejor asesoramiento. </p>       
                    </div>
                    <div class="col-lg-4" align="center">
	                	<img src="images/servir.png" alt="Cercanía que nutre" >       
                        <p class="cercania-1">No basta con llegar a tiempo o servir con la mejor atención, estamos comprometidos en hacerlo bajo los valores y virtudes que nos rigen.</p>
                    </div>
                    <div class="col-lg-4" align="center">
	                	<img src="images/generar.png" alt="Cercanía que nutre" >       
                        <p class=" cercania-1">Cada asesoría y servicio, no se comparan con el orgullo que nos genera tener clientes satisfechos que depositan su confianza en nosotros.</p>
                    </div>
                </div>
                
            
			</div>
			
		</div>

        
        
       
             
        



	</main>
	
<?php 
$ancla = !isset($_REQUEST['anchor']) ? "" : $_REQUEST['anchor'] ;

/*$GLOBALS['scripts'] .= '

<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>


<script>
function openLinea(evt, lineaName) {
  var i, tabcontent-1, tablinks-1;
  tabcontent-1 = document.getElementsByClassName("tabcontent-1");
  for (i = 0; i < tabcontent-1.length; i++) {
    tabcontent-1[i].style.display = "none";
  }
  tablinks-1 = document.getElementsByClassName("tablinks-1");
  for (i = 0; i < tablinks-1.length; i++) {
    tablinks-1[i].className = tablinks-1[i].className.replace(" active", "");
  }
  document.getElementById(lineaName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>


	<script src="css-js/home.js"></script>
	<script>
		$(function(){ 
			setTimeout(function(){
				var strAncla = "#'.$ancla.'";
				$("body,html").stop(true,true).animate({
					scrollTop: $(strAncla).offset().top
				},1000);
			},0)
		})
	</script>
	<script>
	$(document).ready(function(){
		var ancho = $(window).width();
		if (ancho < 450) {
			$("#cobertura-nosotros").attr("src","img/cobertura-movil-01.jpg");
		}
	});
	</script>
	<!-- Owl Carousel -->
	<script src="plugins/owl-carousel/owl.carousel.js"></script>
	<script>
		$(document).ready(function() {
			$("#owl-multi").owlCarousel({
				items : 5,
				itemsDesktop : [1199,3],
				itemsDesktopSmall : [979,3],
				navigation : true,
				pagination : false,
				autoPlay: 3000, //Set AutoPlay to 3 seconds
				theme: "owl-multi"
			});		
		});
	</script>
	
'; */?>
<?php include('includes/footer.php'); ?>