<?php 
$GLOBALS['scripts'] = ''; 
include ('includes/parametros.php');

include_once ('bo/common.php');


?>
<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<?php $header_title = $header_title ? $header_title : 'Alpesur';?>
	<title><?=$header_title;?></title>
	<meta name="description" content="<?= $header_description ?>">
	<meta name="keywords" content="alpesur">
	<meta name="robots" content="index,follow">
	<meta name="robots" content="noodp,noydir">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.5">
	<meta name="format-detection" content="telephone=no">
	<meta name="skype_toolbar" content="skype_toolbar_parser_compatible">
	<meta name="theme-color" content="#000000">

	<!-- SEO Open Graph. -->
	<meta property="og:type" content="website" />
	<meta property="og:title" content="<?php echo $header_title;?>" />
	<meta property="og:description" content="<?php echo $header_description;?>" />
	<meta property="og:url" content="<?=ABS_HTTP_URL?>" />
	<meta property="og:image" content="<?=ABS_HTTP_URL?>images/logo_seo.jpg"/>
	<!-- Rutas -->
	<base href="<?=ABS_HTTP_URL?>">
	<link rel="canonical" href="<?=$base_url;?>">
	<link rel="shortcut icon" href="favicon.ico">
	
	<!-- Fancybox 3 -->
	<link rel="stylesheet" href="plugins/fancybox/jquery.fancybox.min.css">
	<!-- CSS de Plugins -->
	<link rel="stylesheet" href="plugins/font-awesome/font-awesome.css">
	<!-- Tootltipser -->
	<link rel="stylesheet" href="css-js/tooltipster/css/tooltipster.css" />
	<link rel="stylesheet" href="css-js/tooltipster/css/themes/tooltipster-punk.css" />
	<!-- CSS Generales -->
	<link rel="stylesheet" href="css-js/styles.css">
	<link rel="stylesheet" href="bootstrap/bootstrap.css">
	<link rel="stylesheet" href="plugins/dlmenumovil/dlmenumovil.css">
	<!-- <link rel="stylesheet" href="https://file.myfontastic.com/Zi9VUJEA8p7mDCiqzcuC/icons.css"> -->

	<!--[if lt IE 9]>
		<script src="css-js/respond.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js";></script>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
	<!-- google Analytics -->

	<!-- <script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-91777143-1']);
	_gaq.push(['_trackPageview']);
	(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
	</script> -->


	<!-- Global site tag (gtag.js) - Google Analytics --> <script async src="
	https://www.googletagmanager.com/gtag/js?id=UA-91777143-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-91777143-1');
	</script>
	

	