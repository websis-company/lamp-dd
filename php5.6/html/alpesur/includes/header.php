</head>
<body>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.8";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	<header class="site-header" id="headerScroll">
		<?php 
		#-- SEO H1 index
		$file = Util_String::getFile();
		if($file == 'index.php'){
			echo '<h1 class="brand-wrapper">';
		}
		?>
			<a href="index.php" class="brand" title="Alpesur" style="text-indent:-99999px;">
				<?php if($file == 'index.php'){?>
				<span>Alimento balanceado para animales Alpesur</span>
				<?php }//end if ?>
			</a>
		<?php 
		if($file == 'index.php'){
			echo '</h1>';
		}
		?>
		<!-- Top Bar. Begin -->
		<div class="top-bar">
			<div class="re-central">
				<div class="row">
					<div class="col-lg-4 hidden-md hidden-xs"></div>
					<div class="col-lg-8 text-right hidden-md hidden-xs">
						<ul class="header-social">
							<li>
								<a href="http://148.223.205.156:55000/PortalCfdi/wfrLogin.aspx" title="Clientes" class="">CLIENTES</a>
							</li>
							<li>
								<a href="http://148.223.205.150/recepcionhoyCFDI/portal/proveedores/acceso/" title="Proveedores" class="">PROVEEDORES</a>
							</li>
							<li>
								<a href="https://www.gporres.com.mx/colaboradores.html" title="Colaboradores" class="">COLABORADORES</a>
							</li>
							<!-- <li><a href="#" class="icon-facebook" target="_blank" title="facebook"></a></li>
							<li><a href="#" class="icon-twitter" target="_blank" title="twitter"></a></li>
							<li><a href="#" class="icon-youtube" target="_blank" title="youtube"></a></li> -->
						</ul>
						<!-- <ul class="header-language child-inline">
							<li class="expand">
								<a href="contacto" title="Contacto" class="hidden-xs">Facturación</a>
								<ul class="dl-submenu">
									<li><a href="construccion.php">Clientes</a></li>
									<li><a href="construccion.php#partners">Proveedores</a></li>
								</ul> 
							</li>
						</ul> -->
					</div>
				</div>
			</div>
		</div>
		<!-- Top Bar. End -->

		<!-- Header Bottom. Begin -->
		<div class="re-central text-right">
			<div class="row">
				<div class="col-xs-6 col-xs-offset-6">
				</div>
			</div>
		</div>
		<!-- Header Bottom. End -->

		<!-- Cookbook. Begin -->
		<!-- <a href="recetario" class="cookbook-fixed"> -->
		<!-- <a data-toggle="modal" data-target=".bs-recetario-lg" class="cookbook-fixed" style="cursor: pointer;" title="Recetario La Italiana">
			<img src="images/layout/cookbook.png" alt="Cookbook" class="cookbook-fixed-image">
		</a> -->
		<!-- Cookbook. End -->
